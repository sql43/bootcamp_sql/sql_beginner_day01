/*
    RIGHT JOIN
 Используем RIGHT JOIN, так как нам необходимо заполнить заполнить person_order
 именем и возрастом заказчика.
 */
SELECT person_order.order_date, concat(person.name::text, ' (age:', person.age, ')') AS person_information
FROM person RIGHT JOIN person_order
ON person.id = person_order.person_id
ORDER BY order_date, person_information;