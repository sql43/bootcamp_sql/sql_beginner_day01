/*
     NATURAL JOIN
 Cоздаёт неявное объединение на основе одинаковых имён столбцов в объединяемых таблицах.
 Если явно не указать тип соединения, то будет INNER JOIN. Если вывести все столбцы (SELECT *),
 то выведит все общие столбцы в обеих таблицах тоже, с одинаковыми именами.
 Шаблон:
     -------------------------------------
     SELECT *
     FROM T1
     NATURAL [INNER, LEFT, RIGHT] JOIN T2;
     -------------------------------------
 В данном задании главное изменить person.id на person.person_id, чтобы произошло объединение
 по этим элементам(столбцам).
 */
SELECT person_order.order_date, concat(person.name::text, ' (age:', person.age, ')') AS person_information
FROM (SELECT id AS person_id, name, age
    FROM person) AS person
NATURAL RIGHT JOIN person_order
ORDER BY order_date, person_information;