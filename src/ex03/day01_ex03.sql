/*
    ПОИСК СТРОК С ОБЩИМИ ДАННЫМИ В ДВУХ ТАБЛИЦАХ
 INTERSECT - возращает строки, содержащие общие данные для обоих источников строк.
 Для этого необходимо сравнивать одинаковое количество элементов(стообцов) одного типа из обеих таблиц.
 По умолчанию дубликаты строк не возращаеются.
 */
SELECT visit_date AS action_date, person_id FROM person_visits
WHERE (person_id, visit_date) in (
    SELECT  person_id, visit_date from person_visits
    INTERSECT
    SELECT person_id, order_date from person_order
    )
ORDER BY action_date, person_id DESC;
