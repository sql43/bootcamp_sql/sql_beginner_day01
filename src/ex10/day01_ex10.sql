/*
    GLOBAL JOIN
 Шаблон:
    --------------------------------------
    SELECT t1.col, t3.col
    FROM table1
        join table2 ON table1.primarykey = table2.foreignkey
        join table3 ON table2.primarykey = table3.foreignkey
    --------------------------------------
 */
SELECT person.name AS person_name, menu.pizza_name AS pizza_name, pizzeria.name AS pizzeria_name
FROM person_order
    LEFT JOIN person ON person_order.person_id = person.id
    LEFT JOIN menu ON person_order.menu_id = menu.id
    LEFT JOIN pizzeria ON menu.pizzeria_id = pizzeria.id
ORDER BY person_name, pizza_name, pizzeria_name;