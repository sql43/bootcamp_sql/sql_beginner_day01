/*
 В таблицу person_order вставляем колонку название пицц по menu_id.
 Далее объединяем назавние колонок pizza_name по двум таблицам и убираем дубли и сортирвка по DESC.
 */
SELECT pizza_name FROM menu
UNION
(SELECT menu.pizza_name
FROM menu, person_order
WHERE menu.id = person_order.menu_id)
ORDER BY pizza_name DESC;