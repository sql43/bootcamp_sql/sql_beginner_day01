/*
 Объединение строки из нескольких таблиц в одну комбинированную таблицу можно с помощью
 операции над множеством UNION ALL. Количеств колонек должны быть равны при слиянии.
 */
SELECT id AS object_id, pizza_name AS object_name FROM menu
UNION ALL
SELECT id, name FROM person
ORDER BY object_id, object_name;
